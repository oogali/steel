require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module NewApp
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Configure path for jobs
    config.autoload_paths << Rails.root.join('app/jobs').to_s

    # Add our lib/ directory
    config.autoload_paths << Rails.root.join('lib').to_s

    # Enable sidekiq for activejob
    config.active_job.queue_adapter = :sidekiq

    # Set our SASS options
    config.sass.preferred_syntax = :sass

    unless Rails.application.secrets.redis_url.blank?
      config.cache_store = :redis_store,
                           Rails.application.secrets.redis_url,
                           { namespace: 'newapp::rails.cache', expires_in: 15.minutes }
    end

    config.generators.javascript_engine :js
  end
end
