if Rails.application.secrets.librato_email and Rails.application.secrets.librato_api_key
  require 'metriks/reporter/librato_metrics'
  $metriks_reporter = Metriks::Reporter::LibratoMetrics.new(
    Rails.application.secrets.librato.email,
    Rails.application.secrets.librato.api_key
  )
else
  require 'metriks/reporter/logger'
  $metriks_reporter = Metriks::Reporter::Logger.new
end

if $metriks_reporter
  $metriks_reporter.start
  Rails.logger.info("#{$metriks_reporter.class.name} started")
end
