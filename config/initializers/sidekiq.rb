require 'sidekiq'
require 'sidekiq-status'

Sidekiq.configure_server do |config|
  config.error_handlers << Proc.new { |exception, context| Raven.capture_exception(exception, context) }
  config.redis = { url: Rails.application.secrets.redis_url, namespace: 'newapp::jobs' }

  config.client_middleware do |chain|
    chain.add Sidekiq::Status::ClientMiddleware, expiration: 30.minutes
  end

  config.server_middleware do |chain|
    chain.add Sidekiq::Status::ServerMiddleware, expiration: 30.minutes
  end
end

Sidekiq.configure_client do |config|
  config.redis = { url: Rails.application.secrets.redis_url, namespace: 'newapp::jobs' }
  config.client_middleware do |chain|
    chain.add Sidekiq::Status::ClientMiddleware, expiration: 30.minutes
  end
end
