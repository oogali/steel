unless Rails.application.secrets.sparkpost_api_key.blank?
  SparkPostRails.configure do |c|
    c.api_key = Rails.application.secrets.sparkpost_api_key
  end
end
