if defined?(Rack::MiniProfiler) and not Rails.env.production?
  unless Rails.application.secrets.redis_url.blank?
    require_relative './redis'
  end

  require 'rack-mini-profiler'

  Rack::MiniProfiler.config.position = 'right'
  Rack::MiniProfiler.config.skip_schema_queries = true
  unless Rails.application.secrets.redis_url.blank?
    Rack::MiniProfiler.config.storage_options = $redis.client.options
    Rack::MiniProfiler.config.storage = Rack::MiniProfiler::RedisStore
  end
end
