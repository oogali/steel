# Be sure to restart your server when you modify this file.

unless Rails.application.secrets.redis_url.blank?
  $redis = Redis.new url: Rails.application.secrets.redis_url
end
