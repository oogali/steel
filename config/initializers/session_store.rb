# Be sure to restart your server when you modify this file.

if !Rails.application.secrets.redis_url.blank?
  Rails.application.config.session_store :redis_store,
                                         servers: Rails.application.secrets.redis_url,
                                         namespace: 'newapp::rails.session',
                                         expires_in: 15.minutes
else
  Rails.application.config.session_store :cookie_store, key: '_newapp_session'
end
