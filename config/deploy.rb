# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'newapp'
set :repo_url, 'git@bitbucket.org:oogali/newapp.git'
set :vhost, 'newapp.com'
set :port, 3000

set :use_sudo, false
set :group_writable, false
set :deploy_to, File.join('/home/oogali/apps', fetch(:application))
set :deploy_via, :remote_cache
set :deploy_user, ->{ fetch(:user) }
set :copy_strategy, :export
set :normalize_asset_timestamps, false

set :rbenv_type, :user
set :rbenv_ruby, (File.read(File.expand_path(File.join(File.dirname(__FILE__), '..', '.ruby-version'))).strip rescue '2.2.3')
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_setup_shell, true
set :rbenv_install_bundler, true
set :rbenv_install_dependencies, false

set :bundle_flags, "--deployment --quiet --binstubs --shebang ruby-local-exec"

set :keep_releases, 5
set :keep_assets, 2

set :conditionally_migrate, ENV.has_key?('FORCE_MIGRATIONS') ? false : true

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sessions', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
set :linked_files, fetch(:linked_files, []).push('config/secrets.yml')

if ENV['DATABASE_URL']
  set :default_env, fetch(:default_env, {}).update('DATABASE_URL' => ENV['DATABASE_URL'])
end

def process_template(filename)
  require 'erb'

  template = File.read(File.join(File.dirname(__FILE__), '..', filename))
  ERB.new(template).result(binding)
end

def upload_env_vars!(env_vars = {})
  env_dir = File.join('/var/service', "rails-#{fetch(:application).gsub('-', '_')}", 'env')

  env_vars.each do |k, v|
    env_file = File.join(env_dir, k.to_s.upcase)

    content_stream = StringIO.new(v)
    upload!(content_stream, env_file)
    execute :chmod, '+r', env_file
  end
end

def run_svc(arg)
  target = File.join('/service', "rails-#{fetch(:application).gsub('-', '_')}")
  sudo :svc, arg, target
end

namespace :deploy do
  before 'deploy:check:linked_files', 'config:push'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :published, :create_nginx_config do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      content = process_template('config/deploy/templates/nginx.conf.erb')
      content_stream = StringIO.new(content)
      upload!(content_stream, File.join(shared_path, 'config', 'nginx.conf'))
    end
  end

  namespace :daemontools do
    after :published, :create_service do
      on roles(:web), in: :groups, limit: 3, wait: 10 do |host|
        target = File.join('/var/service', "rails-#{fetch(:application).gsub('-', '_')}")
        execute :mkdir, '-p', target
        execute :mkdir, '-p', File.join(target, 'env')
        execute :mkdir, '-p', File.join(target, 'log')
        execute :mkdir, '-p', File.join(target, 'log', 'main')

        as 'root' do
          execute :chown, "#{host.user}:nobody", File.join(target, 'log', 'main')
        end

        execute :chmod, '775', File.join(target, 'log', 'main')

        content = process_template('config/deploy/templates/daemontools-run.sh.erb')
        content_stream = StringIO.new(content)
        upload!(content_stream, File.join(target, 'run'))
        execute :chmod, '+x', File.join(target, 'run')

        content = process_template('config/deploy/templates/daemontools-log-run.sh.erb')
        content_stream = StringIO.new(content)
        upload!(content_stream, File.join(target, 'log', 'run'))
        execute :chmod, '+x', File.join(target, 'log', 'run')
      end
    end

    after :create_service, :set_env_vars do
      on roles(:web), in: :groups, limit: 3, wait: 10 do |host|
        env_vars = {
          APP_DIR:    File.join(fetch(:deploy_to), 'current'),
          HOME:       File.join('/home', host.user),
          RAILS_ENV:  fetch(:stage).to_s
        }

        env_dir = File.join('/var/service', "rails-#{fetch(:application).gsub('-', '_')}", 'env')
        if not test "[ ! -z #{File.join(env_dir, 'SECRET_KEY_BASE')} ]"
          env_vars[:SECRET_KEY_BASE] = %x[ bundle exec rake secret ]
        end

        if ENV['DATABASE_URL']
          env_vars[:DATABASE_URL] = ENV['DATABASE_URL']
        end

        upload_env_vars!(env_vars)
      end
    end

    task :start do
      on roles(:web), in: :groups, limit: 3, wait: 10 do |host|
        run_svc '-u'
      end
    end

    task :stop do
      on roles(:web), in: :groups, limit: 3, wait: 10 do |host|
        run_svc '-d'
      end
    end

    task :restart do
      on roles(:web), in: :groups, limit: 3, wait: 10 do |host|
        run_svc '-tu'
      end
    end
  end

  task :upload do
    on roles(:all) do
      fetch(:linked_files, []).each do |_filename|
        upload!(_filename, File.join(shared_path, _filename))
      end
    end
  end

  after 'deploy:symlink:shared', :symlink_apps_dir do
    on roles(:all) do
      execute :mkdir, '-p', '/apps'

      target = File.join('/apps', fetch(:application))
      next if test "[ -L #{target} ]"
      execute :rm, "-rf", target if test "[ -d #{target} ]"
      execute :ln, "-s", deploy_to, target
    end
  end
end
