source 'https://rubygems.org'

gem 'rails', '4.2.7.1'
gem 'pg'
gem 'sqlite3'
gem 'sass-rails', '~> 5.0'
gem 'haml-rails'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'bcrypt', '~> 3.1.7'
gem 'pry'
gem 'hiredis'
gem 'redis', require: %W(redis/connection/hiredis redis)
gem 'redis-rails'
gem 'metriks'
gem 'unicorn-rails'
gem 'rack-handlers'
gem 'clockwork', require: %W(clockwork clockwork/database_events), github: 'oogali/clockwork', branch: 'selectively-ignore-model-attributes'
gem 'sentry-raven'
gem 'sidekiq'
gem 'sidekiq-status'
gem 'sinatra', require: false
gem 'react-rails'
gem 'devise'
gem 'faraday'
gem 'nokogiri'
gem 'twilio-ruby'
gem 'ledermann-rails-settings'
gem 'redis-namespace'
gem 'oj'
gem 'sparkpost_rails'
gem 'addressable', require: 'addressable/uri'
gem 'momentjs-rails'
gem 'rolify'
gem 'decent_exposure'

group :development do
  gem 'capistrano'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-rbenv'
  gem 'capistrano-rbenv-install'
  gem 'capistrano-upload-config'
  gem 'capistrano-rails-console'
  gem 'rack-mini-profiler', require: 'rack-mini-profiler'
  gem 'byebug'
  gem 'web-console', '~> 2.0'
  gem 'spring-commands-rspec'
end

group :development, :test do
  gem 'pry-rails'
  gem 'spring'
  gem 'rspec-rails'
  gem 'faker'
end

group :test do
  gem 'rspec-sidekiq'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'codecov', require: false
end
