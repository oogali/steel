namespace :scheduler do
  desc 'Start the application scheduler'
  task run: :environment do
    scheduler = JobScheduler.new
    scheduler.run!
  end
end