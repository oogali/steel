<% if namespaced? -%>
require_dependency "<%= namespaced_path %>/application_controller"

<% end -%>
<% module_namespacing do -%>
class <%= controller_class_name %>Controller < ApplicationController
  expose :<%= plural_table_name %>, ->{ <%= orm_class.all(class_name) %> }
  expose :<%= singular_table_name %>

  # GET /<%= plural_table_name %>
  # GET /<%= plural_table_name %>.json
  def index
  end

  # GET /<%= plural_table_name %>/1
  # GET /<%= plural_table_name %>/1.json
  def show
  end

  # GET /<%= plural_table_name %>/new
  def new
  end

  # GET /<%= plural_table_name %>/edit
  def edit
  end

  # POST /<%= plural_table_name %>
  # POST /<%= plural_table_name %>.json
  def create
    if <%= orm_instance.save %>
      redirect_to <%= singular_table_name %>, notice: <%= "'#{human_name} was successfully created.'" %>
    else
      render :new
    end
  end

  # PATCH/PUT /<%= plural_table_name %>/1
  # PATCH/PUT /<%= plural_table_name %>/1.json
  def update
    if <%= orm_instance.update("#{singular_table_name}_params") %>
      redirect_to <%= singular_table_name %>, notice: <%= "'#{human_name} was successfully updated.'" %>
    else
      render :edit
    end
  end

  # DELETE /<%= plural_table_name %>/1
  # DELETE /<%= plural_table_name %>/1.json
  def destroy
    <%= orm_instance.destroy %>
    redirect_to <%= index_helper %>_url, notice: <%= "'#{human_name} was successfully deleted.'" %>
  end

  private

  def <%= "#{singular_table_name}_params" %>
    <%- if attributes_names.empty? -%>
    params.fetch(:<%= singular_table_name %>, {})
    <%- else -%>
    params.require(:<%= singular_table_name %>).permit(<%= attributes_names.map { |name| ":#{name}" }.join(', ') %>)
    <%- end -%>
  end
end
<% end -%>
