require 'benchmark'
require 'clockwork'
require 'clockwork/database_events'

class JobScheduler
  def initialize
    @manager = Clockwork::DatabaseEvents::Manager.new
    Clockwork.manager = @manager

    Clockwork.configure do |config|
      config[:thread] = true
    end

    Clockwork.error_handler do |exception|
      if defined?(Raven)
        # TODO: Add context/parameters that led up to this exception
        Raven.capture_exception(exception)
      end
    end
  end

  def run!
    Clockwork.sync_database_events model: ScheduledTask, from_database: true, every: 10.seconds do |task|
      if not task.recurring? and task.executed_at?
        next
      end

      # TODO: Refactor into a single valid? call (needs to be implemented in model)
      unless task.klass
        @manager.error "Skipping '#{job.name}' because no job class is defined"
        next
      end

      if not task.can_run?
        @manager.log "Skipping '#{task.name}' because task is time restricted"
        next
      end

      # TODO: Pass arguments to our job
      # job_options = { task_name: task.name, logger: @manager.config[:logger] }
      # job_options.update!(task.options.inject({}) do |options, option|
      #   options[option.key] = option.value
      #   options
      # end)

      # Instantiate our class
      job = Object::const_get(task.klass)
      job_args = task.args

      method = nil
      method ||= :perform_later if job.methods.include?(:perform_later)
      method ||= :perform_async if job.methods.include?(:perform_async)
      if not method
        job = job.new
        method ||= :perform
      end

      # Record timing information on how long it takes for job to run
      timing = Benchmark.measure do
        job.send(method, *job_args)
      end

      # Update our job record with time of last execution
      # TODO: Create a history record with job information, return value,
      #       and timing
      task.record_timestamps = false
      task.update!(executed_at: Time.now.utc)

      # TODO: Evaluate whether this can be removed
      # if not obj.last_exception.blank?
      #   self.manager.log "Job '#{job.name}' encountered an exception (#{obj.last_exception[:klass].to_s})"
      #   self.manager.log obj.last_exception.to_json
      # end

      # Log some basic information
      @manager.log "Job '#{task.name}' ran at #{task.executed_at} for #{timing.real.to_f} seconds"

      # TODO: Add ability to send execution timing metrics to a sink such as Graphite, Librato, etc. ()Preferably via eric/metriks)
    end

    Clockwork.run
  end
end
