#!/usr/bin/env bash

source /etc/profile.d/rbenv.sh
exec 2>&1

BUNDLE_PATH=${BUNDLE_PATH:-/bundle}
APP_USER=${APP_USER:-admin@nowhere.com}
APP_PASSWORD=${APP_PASSWORD:-admin}

cd /app || exit 1

bundle check || bundle install

if [ ! -z "${APP_DB_MIGRATE}" ] && [ "${APP_DB_MIGRATE}" == "1" ]; then
  bundle exec rake db:migrate
fi

if [ ! -z "${APP_CREATE_USER}" ] && [ "${APP_CREATE_USER}" == "1" ]; then
  bundle exec rails runner "User.where(email: '${APP_USER}').first_or_create(password: '${APP_PASSWORD}', password_confirmation: '${APP_PASSWORD}', confirmed_at: Time.now)"
fi

exec bundle exec "$@"
