#!/usr/bin/env bash

if [ $# -ne 1 ]; then
    echo "${0} <ProjectName>"
    exit 1
fi

FILES=(app/views/application/_nav.html.haml
    app/views/layouts/application.html.haml
    config/application.rb
    config/database.yml
    config/deploy.rb
    config/initializers/redis.rb
    config/initializers/sidekiq.rb
    config/initializers/session_store.rb
    db/sql-scripts/001_create_db.sql
    docker-compose.yml
    lib/steel/mailer.rb
    lib/tasks/steel.rake
    README.md)

CamelCase=${1}
lowercase=$(echo "${CamelCase}" | tr -s '[:upper:]' '[:lower:]')
UPPERCASE=$(echo "${CamelCase}" | tr -s '[:lower:]' '[:upper:]')

for f in "${FILES[@]}" ; do
    sed -i '' -e "s/NewApp/${CamelCase}/g; s/newapp/${lowercase}/g; s/NEWAPP/${UPPERCASE}/g" "${f}"
done

# rename application lib directory
git mv lib/steel "lib/${lowercase}"
git mv lib/tasks/steel.rake "lib/tasks/${lowercase}.rake"

# delete old origin
git remote rm origin

# delete this init script
git rm -f "${0}"

# truncate git history, and perform initial commit
git branch -m master initial
git checkout --orphan master
git commit -a -m "Initial creation of ${CamelCase}"

# delete old temporary branch, and display git log
git branch -D initial
git log

# install gems
BUNDLE_JOBS=2 bundle install
