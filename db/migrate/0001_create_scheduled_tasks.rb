class CreateScheduledTasks < ActiveRecord::Migration
  def change
    create_table :scheduled_tasks do |t|
      t.integer :quantity
      t.string :period
      t.string :name
      t.string :klass
      t.jsonb :args
      t.string :at
      t.string :tz
      t.boolean :recurring
      t.datetime :not_before
      t.datetime :not_after
      t.datetime :executed_at

      t.timestamps null: false
    end
  end
end
