class ScheduledTask < ActiveRecord::Base
  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validates :klass, presence: true
  validates :quantity, presence: true, if: :recurring?
  validates :period, presence: true, if: :recurring?

  def frequency
    return nil if quantity.blank?

    quantity.send(period.pluralize)
  end

  def recurring_interval
    return nil unless recurring?

    "#{quantity} #{period.pluralize(quantity)}"
  end

  def tz
    tz? ? read_attribute(:tz) : Time.zone.name
  end

  def can_run_at?(ts=nil)
    raise Exception('Must provide a valid Time object') if ts.nil?
    return true if not_before.blank? and not_after.blank?

    return false if not_before? and ts < not_before
    return false if not_after? and ts > not_after

    true
  end

  def can_run?
    can_run_at?(Time.now)
  end

  def if?(ts=nil)
    if not recurring?
      return false if executed_at?
    end

    can_run_at?(ts)
  end

  def ignored_attributes
    [ :executed_at ]
  end
end
