class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_settings do |s|
    s.key :profile, defaults: { name: nil, time_zone: 'Eastern Time (US & Canada)' }
  end

  def name
    settings(:profile).name
  end

  def name=(new_name)
    settings(:profile).name = new_name
  end

  def time_zone
    settings(:profile).time_zone
  end

  def time_zone=(new_tz)
    settings(:profile).time_zone = new_tz
  end

  def display_name
    name || email
  end

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end
end
