class ScheduledTasksController < ApplicationController
  before_action :set_scheduled_task, only: [:show, :edit, :update, :destroy, :clear_execution]

  # GET /scheduled_tasks
  # GET /scheduled_tasks.json
  def index
    @scheduled_tasks = ScheduledTask.all
  end

  # GET /scheduled_tasks/1
  # GET /scheduled_tasks/1.json
  def show
  end

  # GET /scheduled_tasks/new
  def new
    @scheduled_task = ScheduledTask.new
  end

  # GET /scheduled_tasks/1/edit
  def edit
  end

  # POST /scheduled_tasks
  # POST /scheduled_tasks.json
  def create
    @scheduled_task = ScheduledTask.new(scheduled_task_params)

    respond_to do |format|
      if @scheduled_task.save
        format.html { redirect_to @scheduled_task, notice: 'Scheduled task was successfully created.' }
        format.json { render :show, status: :created, location: @scheduled_task }
      else
        format.html { render :new }
        format.json { render json: @scheduled_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /scheduled_tasks/1
  # PATCH/PUT /scheduled_tasks/1.json
  def update
    respond_to do |format|
      if @scheduled_task.update(scheduled_task_params)
        format.html { redirect_to @scheduled_task, notice: 'Scheduled task was successfully updated.' }
        format.json { render :show, status: :ok, location: @scheduled_task }
      else
        format.html { render :edit }
        format.json { render json: @scheduled_task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /scheduled_tasks/1
  # DELETE /scheduled_tasks/1.json
  def destroy
    @scheduled_task.destroy
    respond_to do |format|
      format.html { redirect_to scheduled_tasks_url, notice: 'Scheduled task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /scheduled_tasks/1/clear_execution
  # POST /scheduled_tasks/1/clear_execution.json
  def clear_execution
    @scheduled_task.update(executed_at: nil)
    respond_to do |format|
      format.html { redirect_to @scheduled_task, notice: 'Scheduled task was successfully cleared of its last execution time.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_scheduled_task
      @scheduled_task = ScheduledTask.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def scheduled_task_params
      params.require(:scheduled_task).permit(:quantity, :period, :name, :klass, {args: []}, :at, :tz, :recurring, :not_before, :not_after, :executed_at)
    end
end
