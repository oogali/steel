class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_raven_context!

  # Uncomment when devise is activated
  # before_action :authenticate_user!
  # around_action :set_time_zone

  def set_raven_context!
    # Uncomment when devise is activated
    # Raven.user_context(id: current_user.try(:id), email: current_user.try(:email))
    Raven.extra_context(params: params.to_hash, url: request.url)
  end

  def get_user_time_zone
    return 'Eastern Time (US & Canada)' if not user_signed_in?
    current_user.settings(:profile).time_zone
  end

  def set_time_zone(&block)
    Time.use_zone(get_user_time_zone, &block)
  end

  def json_request?
    request.format.json?
  end

  def options_request?
    request.method == :options
  end
end
