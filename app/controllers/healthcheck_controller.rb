class HealthcheckController < ApplicationController
  skip_before_action :authenticate_user!
  skip_around_action :set_time_zone

  respond_to :plain

  def status
    render plain: 'Application OK'
  end
end
