class ScheduledTaskRecurringIntervalValidator < ActiveModel::Validator
  def validate(record)
    if record.recurring?
      if record.quantity.blank? or record.frequency.blank?
        record.errors.add(:base, 'must have a recurring interval defined to have a recurring task')
      end
    end
  end
end