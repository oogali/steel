class TaskArguments extends React.Component {
    constructor(props: any) {
        super(props);

        this.state = {
            arguments: props.arguments,
        };

        this.onRemove = this._onRemove.bind(this);
        this.addArgument = this._addArgument.bind(this);
    }

    _onRemove(arg) {
        new_arguments = this.state.arguments.slice().filter((elem, idx, ary) => {
            return arg != elem;
        });

        this.setState({
            arguments: new_arguments
        });
    }

    _addArgument() {
        new_arguments = this.state.arguments.slice();
        new_arguments.push(null);
        this.setState({
            arguments: new_arguments
        });
    }

    render() {
        let { className, formName, fieldName, placeholder } = this.props;
        let arguments = this.state.arguments;

        return (
            <div>
                {arguments.map((argument, idx) => {
                    let field_id = formName + '_' + fieldName;
                    let field_name = formName + '[' + fieldName + ']' + '[]';

                    return (
                        <TaskArgument
                            key={'taskArgument_' + idx}
                            idx={idx}
                            className={className}
                            id={field_id}
                            name={field_name}
                            value={argument}
                            placeholder={placeholder}
                            onRemove={this.onRemove}
                        />
                    );
                })}

                <div className="field pure-control-group">
                    <label />

                    &nbsp;

                    <button
                        className="pure-button"
                        type="button"
                        onClick={this.addArgument}
                    >

                        <i className="fa fa-lg fa-plus"></i>
                        &nbsp;Add
                    </button>
                </div>
            </div>
        );
    }
}

TaskArguments.propTypes = {
    className: React.PropTypes.string,
    formName: React.PropTypes.string.isRequired,
    fieldName: React.PropTypes.string.isRequired,
    arguments: React.PropTypes.array,
    placeholder: React.PropTypes.string,
};

TaskArguments.defaultProps = {
    className: 'pure-input-1-2',
    formName: 'scheduled_task',
    fieldName: 'args',
    arguments: [],
    placeholder: '(task argument...)',
};