class TaskArgument extends React.Component {
    render() {
        let { idx, className, id, name, value, placeholder, onRemove } = this.props;

        return (
            <div
                className="field pure-control-group"
                key={id + '_' + idx}>

                <label for={name}>
                    {idx == 0 ? 'Job Arguments': ''}
                </label>

                &nbsp;

                <input
                    className={className}
                    type="text"
                    id={id}
                    name={name}
                    defaultValue={value}
                    placeholder={placeholder}
                    multiple="multiple"
                />

                &nbsp;

                <button
                    className="pure-button"
                    type="button"
                    onClick={() => onRemove(value)}
                >

                    <i className="fa fa-lg fa-times"></i>
                </button>
            </div>
        );
    }
}

TaskArgument.propTypes = {
    idx: React.PropTypes.number,
    className: React.PropTypes.string,
    id: React.PropTypes.string,
    name: React.PropTypes.string.isRequired,
    value: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    onRemove: React.PropTypes.func,
};

TaskArgument.defaultProps = {
    className: 'pure-input-1-2',
};