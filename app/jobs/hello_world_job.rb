class HelloWorldJob < ApplicationJob
  queue_as :default

  def perform(*args)
    if args
      puts args
    else
      puts 'Hello World!'
    end
  end
end
