module ScheduledTasksHelper
  def periods
    [ :second, :minute, :hour, :day, :week, :month ]
  end

  def jobs
    Dir[File.join(File.dirname(__FILE__), '../jobs/**/*_job.rb')].map { |f| File.basename(f, '.*').camelize }
  end
end
