json.array!(@scheduled_tasks) do |scheduled_task|
  json.extract! scheduled_task, :id, :quantity, :period, :name, :klass, :args, :at, :tz, :recurring, :not_before, :not_after, :executed_at
  json.url scheduled_task_url(scheduled_task, format: :json)
end
