json.extract! @scheduled_task, :id, :quantity, :period, :name, :klass, :args, :at, :tz, :recurring, :not_before, :not_after, :executed_at, :created_at, :updated_at
