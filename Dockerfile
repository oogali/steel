# Source from Idle Pattern's Alpine latest with rbenv and Ruby 2.2.3
FROM idlepattern/alpine-rbenv-ruby:2.2.3

# Install additional dependencies for the application
RUN apk --no-cache add nodejs

# Housekeeping/attribution
MAINTAINER "Omachonu Ogali" <oogali@idlepattern.com>

# Add application to container
RUN mkdir -p /app /app/log /app/tmp
WORKDIR /app
ADD . /app/

# Set path for gems and install
ENV BUNDLE_PATH /bundle
ENV BUNDLE_JOBS 2
RUN bash --login -c "RAILS_ENV=production SECRET_KEY_BASE=asset-build DATABASE_URL=sqlite3://tmp/asset-build.db bundle exec rake assets:precompile"
RUN rm -f tmp/asset-build.db

# Precompile static assets
RUN bash --login -c "bundle exec rake assets:precompile"

# Expose our volume
VOLUME ["/app"]

# Set entrypoint
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

ENV APP_DB_MIGRATE false
ENV APP_CREATE_USER false

# Launch server
EXPOSE 30051
CMD rails server -b 0.0.0.0
