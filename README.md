newapp
=====

This is my attempt at formulating a rapid way of creating a new Rails app that incorporates my opinions on what makes an ideal starting point.

System Dependencies
-------------------

### Compiler
You should have a working compiler (OS X: Install XCode, Linux: Install gcc, FreeBSD: clang is part of the base system).

### PostgreSQL
NewApp assumes you're running a system where the PostgreSQL development headers and libraries are already installed.

You'll want PostgreSQL 9.3 or newer.

### rbenv
You should have rbenv installed for managing your different Ruby environments.

### rbenv-gemset
rbenv-gemset is used for managing your gems, this avoids polluting your home directory (and/or system directory) with gem versions that may conflict with other tools that you're using.

This is achieved by managing gems in a directory local to this project called ```./gems```

Ruby
----
NewApp assumes you're using rbenv, and the project version is pinned to Ruby 2.2.3.

If you don't have this version installed, you should install it (caveat: this requires ruby-build, the rbenv installation plugin):

```
rbenv install 2.2.3
```


You'll also need **Bundler** -- if you're unsure whether this is installed, give this a quick whirl:

```
gem install --no-ri --no-rdoc bundler
```

### Installing project dependencies
This is all handled by Bundler:

```
bundle install
```

Configuration
-------------
### Listening Port
By default, this project will bind to ```0.0.0.0:3000```. If you want to change this, modify ```config/boot.rb``` with your preferences.

### Secrets
You should replace the ***development*** and ***test*** secrets in ```config/secrets.yml``` with newly generated ones.

You can generate a new secret by running:

```
bundle exec rake secret
```

Database
--------
### Creation
#### Development and Testing
You'll need to create your databases for your development environments.

```
RAILS_ENV=development bundle exec rake db:create
RAILS_ENV=test bundle exec rake db:create
```

#### Production
On your production database server, you'll need to create your user first (you'll be prompted to set its password during creation):

```
sudo -u pgsql -i createuser -DERPSl newapp
```

Then create your database:

```
sudo -u pgsql -i createdb -O newapp newapp_production
```

### Initialization
This is the same as any other project:

```
bundle exec rake db:seed
```

Services
--------
NewApp assumes you have daemontools installed, and the following:

* svcan is configured to use ```/service``` as its service directory
* Your service definitions live in ```/var/service```
* You symlink your service definitions from ```/var/service``` into ```/service```

```TODO: How to configure a service```

Deployment
----------
NewApp uses Capistrano for deployment.

Before you deploy to a production server, you'll need to make the following changes:

* Change the ***repo_url*** value in ```config/deploy.rb``` to the URL of your Git repo
* Change the ***deploy_to*** value in ```config/deploy.rb``` to the path you want to deploy NewApp in on the remote server
* Set your target servers in ```config/deploy/production.rb```

To deploy:

```
bundle exec cap production deploy
```
