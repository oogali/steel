module ControllerMacros
  def admin_login
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = FactoryGirl.create(:user, :confirmed, :admin)
      sign_in user
    end
  end

  def login
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = FactoryGirl.create(:user, :confirmed)
      sign_in user
    end
  end
end
