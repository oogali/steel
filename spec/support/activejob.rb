require_relative 'job_helpers'

RSpec.configure do |config|
  config.include JobHelpers, type: :job
end
