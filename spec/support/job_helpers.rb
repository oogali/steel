module JobHelpers
  def self.included(base)
    base.before(:each) { ActiveJob::Base.queue_adapter = :test }
  end
end
