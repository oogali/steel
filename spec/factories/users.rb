FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
  end

  trait :confirmed do
    confirmed_at Date.today
  end

  trait :admin do
    after(:create) { |user| user.add_role(:admin) }
  end
end
