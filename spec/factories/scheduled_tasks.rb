FactoryGirl.define do
  factory :scheduled_task do
    quantity 5
    period 'minutes'
    sequence(:name) { |n| "Recurring Task ##{n}" }
    klass 'RecurringTask'
    args ''
    at nil
    tz 'UTC'
    recurring true
    not_before nil
    not_after nil
    executed_at 30.minutes.ago
  end
end
