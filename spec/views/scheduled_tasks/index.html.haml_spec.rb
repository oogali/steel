require 'rails_helper'

RSpec.describe "scheduled_tasks/index", type: :view do
  before(:each) do
    assign(:scheduled_tasks, create_list(:scheduled_task, 2))
  end

  it "renders a list of scheduled_tasks" do
    render
    assert_select "tr>td", text: /Recurring Task #\d+/, count: 2
    assert_select "tr>td", text: 'RecurringTask', count: 2
    assert_select "tr>td", text: /Yes, every \d+ minutes/, count: 2
    assert_select "tr>td", text: '', count: 2
    assert_select "tr>td", text: /\w+ \d+, \d+ \d+:\d+(AM|PM)/, count: 2
  end
end
