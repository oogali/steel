require 'rails_helper'

RSpec.describe "scheduled_tasks/edit", type: :view do
  before(:each) do
    @scheduled_task = assign(:scheduled_task, ScheduledTask.create!(
      :quantity => "",
      :period => "MyString",
      :name => "MyString",
      :klass => "MyString",
      :args => "",
      :at => "MyString",
      :tz => "MyString",
      :recurring => false
    ))
  end

  it "renders the edit scheduled_task form" do
    render

    assert_select "form[action=?][method=?]", scheduled_task_path(@scheduled_task), "post" do

      assert_select "input#scheduled_task_quantity[name=?]", "scheduled_task[quantity]"

      assert_select "select#scheduled_task_period[name=?]", "scheduled_task[period]"

      assert_select "input#scheduled_task_name[name=?]", "scheduled_task[name]"

      assert_select "select#scheduled_task_klass[name=?]", "scheduled_task[klass]"

      assert_select "input#scheduled_task_at[name=?]", "scheduled_task[at]"

      assert_select "input#scheduled_task_tz[name=?]", "scheduled_task[tz]"

      assert_select "input#scheduled_task_recurring[name=?]", "scheduled_task[recurring]"
    end
  end
end
