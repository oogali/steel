require 'rails_helper'

RSpec.describe "scheduled_tasks/show", type: :view do
  before(:each) do
    @scheduled_task = assign(:scheduled_task, create(:scheduled_task))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Recurring Task #\d+/)
    expect(rendered).to match(/RecurringTask/)
    expect(rendered).to match(/Yes, every \d+ minutes/)
    expect(rendered).to match(/no arguments/)
    expect(rendered).to match(/no specific time/)
    expect(rendered).to match(/UTC/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/\w+ \d+, \d+ \d+:\d+:\d+ (AM|PM)/)
  end
end
